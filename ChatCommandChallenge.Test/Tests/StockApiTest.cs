﻿using ChatCommandChallenge.Core.Interfaces;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChatCommandChallenge.Test.Tests
{
    [TestFixture]
    public class StockApiTest
    {
        IStockApiService _stockApiService;
        public StockApiTest(IStockApiService stockApiService)
        {
            _stockApiService = stockApiService;
        }

        [Test]
        public async void GetStockTest()
        {
            string stockCode = "aapl.us";
            string result = await _stockApiService.GetStock(stockCode);
            Assert.IsNotNull(result);
        }
    }
}
