﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatCommandChallenge.Core.Models
{
    [Serializable]
    public class Messages
    {
        public string Text { get; set; }

        public string ConnectionId { get; set; }

        public MessageType Type { get; set; }
    }

    public enum MessageType
    {
        Success = 1,
        Warning = 2,
        Danger = 3
    }
}
