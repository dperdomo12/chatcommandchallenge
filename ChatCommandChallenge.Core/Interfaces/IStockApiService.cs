﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ChatCommandChallenge.Core.Interfaces
{
    public interface IStockApiService
    {
        Task<string> GetStock(string stockCode);
    }
}
