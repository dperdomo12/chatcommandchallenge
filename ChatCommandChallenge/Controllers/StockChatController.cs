﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ChatCommandChallenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockChatController : ControllerBase
    {
        public IActionResult Test()
        {
            return Ok();
        }
    }
}