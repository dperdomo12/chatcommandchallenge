﻿using ChatCommandChallenge.Core.Interfaces;
using ChatCommandChallenge.Core.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ChatCommandChallenge.Core.Services
{
    public class StockApiService : IStockApiService
    {

        private readonly AppSettings settings;
        HttpClient _client;
        public StockApiService(IOptions<AppSettings> appSettings)
        {
            settings = appSettings.Value;
            _client = new HttpClient();
        }

        public async Task<string> GetStock(string stockCode)
        {
            try
            {
                var response = await _client.GetAsync(string.Format(settings.StockApiUrl, stockCode));

                string fileContent = "";

                using (StreamReader sr = new StreamReader(await response.Content.ReadAsStreamAsync()))
                {
                    fileContent = sr.ReadToEnd();
                }

                List<string[]> csvRows = new List<string[]>();

                string[] Rows = fileContent.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string row in Rows)
                {
                    csvRows.Add(row.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
                }
                if (csvRows.Count > 1)
                {
                    if (csvRows[1][6] == "N/D")
                    {
                        return "Invalid stock code.";
                    }
                    return $"{csvRows[1][0]} quote is {csvRows[1][6]} per share";
                }

                return null;
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid stock code.");
            }
        }
    }
}
